{-==== ФАМИЛИЯ ИМЯ, НОМЕР ГРУППЫ ====-}
{-==== ДМИТРИЕВ РОМАН, 11-301 ====-}

module Trouble
       ( rocket
       , Load(..)
       , Spaceship(..)
       , orbiters
       , Phrase(..)
       , finalFrontier
       ) where

import Data.Function
import Data.List

{- Дан список ступеней ракеты, каждая ступень описана
парой «тяга - масса»

   Необходимо оставить в списке только одну ступень, с
максимальным отношением тяги к массе, если масса этой
ступени равна минимальной, иначе вернуть исходный список.

λ> rocket [(120,2),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,2)]
λ> rocket [(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]
[(120,20),(10,2),(60,14),(90,30),(5,2),(10000,9999)]

   Все деления производить нацело.
 -}
rocket :: [(Integer, Integer)] -> [(Integer, Integer)]
minimum' (x1,x2) (y1,y2) = if x2 <= y2 then (x1,x2) else (y1,y2) 
minimumX [x] = x 
minimumX (x:xs) = minimum' x (minimumX xs) 
maximum' (x1,x2) (y1,y2) = if ((x1 `div` x2) >= (y1 `div` y2)) && (x2 <= y2) then (x1,x2) else (y1,y2) 
maximumX [x] = x 
maximumX (x:xs) = maximum' x (maximumX xs)  
rocket [] = []
rocket xs = if (snd (minimumX xs)) == (snd (maximumX xs)) then [maximumX xs] else xs
{- Космический корабль (КК) может состоять из:
 * ракеты, запускающей на заданную высоту несколько КК
 * полезной нагрузки из нескольких:
   - орбитальных аппаратов
   - посадочных зондов
 -}
data Load a = Orbiter a         -- орбитальный аппарат
            | Probe a           -- посадочный зонд
            deriving (Eq,Ord,Show)
data Spaceship a = Rocket Int [Spaceship a] -- высота и список кораблей
                 | Cargo [Load a]           -- полезная нагрузка
                 deriving (Eq,Show)

{- Дан список КК

   Вернуть список всех орбитальных аппаратов

λ> orbiters [Rocket 300 [Rocket 200 [Cargo [Orbiter "LRO", Probe "Lunokhod"]], Cargo [Orbiter "ISS"]]]
[Orbiter "ISS", Orbiter "LRO"]
 -}
orbiters :: [Spaceship a] -> [Load a]
--getOrbiters :: [Load a] -> [Load a]

orbiters [] = []
orbiters ((Cargo x):xs) = (orbiters xs) ++ (showOrb x)
orbiters ((Rocket a x):xs) = (orbiters xs) ++ (orbiters x)

showOrb [] = []
showOrb ((Orbiter x):xs) = (Orbiter x):(showOrb xs)
showOrb ((Probe x):xs) = showOrb xs

{- Даны фразы, нужно выдать того, кто сказал её

   instance Eq - не задан!
-}

data Phrase = Warp Int           -- Kirk:  Warp X, Mr. Sulu
            | BeamUp String      -- Kirk:  Beam X up, Scotty
            | IsDead String      -- McCoy: X is dead, Jim!
            | LiveLongAndProsper -- Spock: Live long and prosper
            | Fascinating        -- Spock: Fascinating!
            deriving Show

{-
λ> finalFrontier [IsDead "He", Fascinating, Warp 3, LiveLongAndProsper]
["McCoy", "Spock", "Kirk", "Spock"]
-}
finalFrontier :: [Phrase] -> [String]
finalFrontier [] = []
finalFrontier ([Fascinating]) = ["Spock"] 
finalFrontier ([Warp a]) = ["Kirk"] 
finalFrontier ([BeamUp s]) = ["Kirk"] 
finalFrontier ([LiveLongAndProsper]) = ["Spock"] 
finalFrontier ([IsDead s]) = ["McCoy"] 

finalFrontier ((IsDead s):as) = ["McCoy"] ++ finalFrontier as
finalFrontier ((Warp a):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((Fascinating):as) = ["Spock"] ++ finalFrontier as
finalFrontier ((BeamUp s):as) = ["Kirk"] ++ finalFrontier as 
finalFrontier ((LiveLongAndProsper):as) = ["Spock"] ++ finalFrontier as 