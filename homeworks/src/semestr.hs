data Term  = Var Char
			| Lambda Char Term
			| Application Term Term

instance Show Term where
	show (Var a) = [a]
	show (Lambda x t) = "(|" ++ show x ++ "." ++ show t ++ ")"
	show (Application f a) = show f ++ " " ++ show a


eval1 :: Term -> Term
eval1 (Lambda name t) = Lambda name t
eval1 (Var name) = Var name
eval1 (Application t1 t2) = case eval1 t1 of
	Lambda name1 t3 -> reduce name1 t2 t3
	result -> Application result t2

eval :: Term -> Term
eval (Lambda name term) = Lambda name term
eval (Var name) = Var name
eval (Application term1 term2)  = case eval term1 of
  										Lambda name term3 -> eval (reduce name term2 term3)
  										result -> Application result term2

reduce :: Char -> Term -> Term -> Term
reduce name1 (Var name2) (Lambda name3 term)
 | name2 == name3 = Lambda name1 (Var name2)
 | otherwise = Lambda name3 (reduce name1 (Var name2) term)
reduce name1 term1 (Lambda name2 term2) 
							| name1 /= name2  = Lambda name2 (reduce name1 term1 term2)
							| otherwise = Lambda name2 term2

reduce name1 term1 (Application term2 term3) = Application (reduce name1 term1 term2) (reduce name1 term1 term3)
reduce name1 term1 (Var name2)
							| name1 == name2     = term1
							| otherwise          = Var name2


f = eval(Application(Application(Application(Lambda 'x'(Lambda 'y' (Var 'x')))(Var 'y')) (Lambda 'z' (Var 'z'))) (Lambda 'u'(Lambda 'v' (Var 'v'))))
f1 = eval1 $ Application (Lambda  'x' $ Lambda  'y' $ Var 'x') (Var 'z')
f2 = eval1 $ Application (Lambda  'x' $ Lambda  'y' $ Var 'x') (Var 'y')